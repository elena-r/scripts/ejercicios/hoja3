Algoritmo ejercicio5
	num <- 0
	Escribir 'Introduce un n�mero mayor que cero'
	Leer num
	Si num>0 Entonces
		resto <- num MOD 2
		Si resto=0 Entonces
			Escribir 'Es par'
		SiNo
			Escribir 'Es impar'
		FinSi
	SiNo
		Escribir 'El n�mero no es correcto'
	FinSi
FinAlgoritmo

